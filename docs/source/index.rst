.. hydra-docss documentation master file, created by
   sphinx-quickstart on Fri Aug 17 18:01:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hydra-docss's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   all-me
   bggg

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
